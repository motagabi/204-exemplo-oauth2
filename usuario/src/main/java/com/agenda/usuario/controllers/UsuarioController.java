package com.agenda.usuario.controllers;
import com.agenda.usuario.security.Usuario;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class UsuarioController {

    @GetMapping("/{nomeUsuario}")
    public Usuario getUsuario(@PathVariable String nomeUsuario, @AuthenticationPrincipal Usuario usuario) {
        if (usuario.getNome().equals(nomeUsuario)) {
            usuario.setEmail("gabimotaoXX@ggg.com");
            usuario.setTelefone("(11) 98511-0844");
            return usuario;
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Usuário não autorizado.");
        }
    }
/*
    @GetMapping("/info")
    public Usuario getUsuario(@AuthenticationPrincipal Usuario usuario) {
        System.out.println(usuario.toString());
        return usuario;
    }

    @PostMapping("/contato")
    public Agenda criarContato(@AuthenticationPrincipal Usuario usuario, @RequestBody Agenda agenda) {
        agenda.setUsuarioId(usuario.getId());
        return agendaService.salvarContato(agenda);
    }

    @GetMapping("/contatos")
    public Iterable<Agenda> exibirContatos(@AuthenticationPrincipal Usuario usuario) {
        return agendaService.getContatosPorUsuarioId(usuario.getId());
    }
 */
}
