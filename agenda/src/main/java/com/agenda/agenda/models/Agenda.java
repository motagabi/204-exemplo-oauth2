package com.agenda.agenda.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Agenda {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private int usuarioId;

    @NotNull(message ="Nome do contato precisa ser preenchido.")
    @Size(min = 3, message = "Nome precisa ter no mínimo 3 letras.")
    @NotBlank(message = "Nome do contato precisa ser preenchido.")
    private String nome;

    @NotNull(message ="Nome do contato precisa ser preenchido.")
    @Size(min = 3, message = "Nome precisa ter no mínimo 3 letras.")
    @NotBlank(message = "Nome do contato precisa ser preenchido.")
    private String contato;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getContato() {
        return contato;
    }

    public void setContato(String contato) {
        this.contato = contato;
    }
}
