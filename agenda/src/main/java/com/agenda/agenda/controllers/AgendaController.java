package com.agenda.agenda.controllers;

import com.agenda.agenda.models.Agenda;
import com.agenda.agenda.security.Usuario;
import com.agenda.agenda.services.AgendaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AgendaController {

    @Autowired
    private AgendaService agendaService;

    @GetMapping("/info")
    public Usuario getUsuario(@AuthenticationPrincipal Usuario usuario) {
        System.out.println(usuario.toString());
        return usuario;
    }

    @PostMapping("/contato")
    public Agenda criarContato(@AuthenticationPrincipal Usuario usuario, @RequestBody Agenda agenda) {
        agenda.setUsuarioId(usuario.getId());
        return agendaService.salvarContato(agenda);
    }

    @GetMapping("/contatos")
    public Iterable<Agenda> exibirContatos(@AuthenticationPrincipal Usuario usuario) {
        return agendaService.getContatosPorUsuarioId(usuario.getId());
    }
}
