package com.agenda.agenda.repositories;

import com.agenda.agenda.models.Agenda;
import org.springframework.data.repository.CrudRepository;

public interface AgendaRepository extends CrudRepository<Agenda, Integer> {

    Iterable<Agenda> findAllByUsuarioId(Integer usuarioId);
}
