package com.agenda.agenda.services;

import com.agenda.agenda.models.Agenda;
import com.agenda.agenda.repositories.AgendaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AgendaService {
    @Autowired
    private AgendaRepository agendaRepository;

    public Agenda salvarContato(Agenda agenda) {
        return agendaRepository.save(agenda);
    }

    public Iterable<Agenda> getContatosPorUsuarioId(int usuarioId) {
        return agendaRepository.findAllByUsuarioId(usuarioId);
    }

}
